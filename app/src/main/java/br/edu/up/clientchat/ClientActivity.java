package br.edu.up.clientchat;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class ClientActivity extends Activity {

  EditText serverIp, smessage;
  TextView chat;
  Button connectPhones, sent;
  String serverIpAddress = "", msg = "", str;
  Handler handler = new Handler();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_client);

    chat = (TextView) findViewById(R.id.chat);
    serverIp = (EditText) findViewById(R.id.server_ip);
    smessage = (EditText) findViewById(R.id.smessage);

    sent = (Button) findViewById(R.id.sent_button);
    sent.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Log.d("CLIENTE", "chamou...");
        Thread sentThread = new Thread(new sentMessage());
        sentThread.start();
      }
    });

    connectPhones = (Button) findViewById(R.id.connect_phones);
    connectPhones.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        serverIpAddress = serverIp.getText().toString();
        if (!serverIpAddress.equals("")) {
          Log.d("Client", "teste....");
          Thread clientThread = new Thread(new ClientThread());
          clientThread.start();
        }
      }
    });
  }

  class sentMessage implements Runnable {
    @Override
    public void run() {
      Log.d("Client", "rodou.....");
      try {
        InetAddress serverAddr = InetAddress.getByName(serverIpAddress);
        Socket socket = new Socket(serverAddr, 10000); //create client socket
        DataOutputStream os = new DataOutputStream(socket.getOutputStream());
        str = smessage.getText().toString();
        str = str + "\n";
        msg += "Client : " + str;
        handler.post(new Runnable() {
          @Override
          public void run() {
            chat.setText(msg);
          }
        });
        os.writeUTF(str);
        os.flush();
        os.close();
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public class ClientThread implements Runnable {
    public void run() {
      try {
        Log.d("Client", "rodou.....");
        while (true) {

          InetAddress serverAddr = InetAddress.getByName(serverIpAddress);
          Socket socket = new Socket(serverAddr, 10000); //create client socket

          DataInputStream in = new DataInputStream(socket.getInputStream());

          msg += "Server : " + in.readUTF();
          handler.post(new Runnable() {
            @Override
            public void run() {
              chat.setText(msg);
            }
          });

          in.close();
          socket.close();
          Thread.sleep(100);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}